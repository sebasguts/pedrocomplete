###############################
##
## Dockerfile for the homalg release
##
## Creates a container with arch linux, Polymake, Singular, GAP, and homalg packages as git repositories.
##
###############################

FROM sebasguts/homalg_release

MAINTAINER Sebastian Gutsche

RUN \
    ## NormalizInterface
    cd /opt/gap4r7/local/pkg && \
    git clone https://github.com/fingolfin/NormalizInterface.git && cd NormalizInterface && \
    git clone https://github.com/csoeger/Normaliz Normaliz.git && ./build-normaliz.sh && ./autogen.sh && ./configure --with-gaproot=/opt/gap4r7 --with-normaliz=$PWD/Normaliz.git/DST && \
    make && \
    ## 4ti2
    cd /tmp && wget http://www.4ti2.de/version_1.6.6/4ti2-1.6.6.tar.gz && tar -xf 4ti2-1.6.6.tar.gz && cd 4ti2-1.6.6 && ./configure --enable-shared && make -j10 && sudo make install && \
    ## 4ti2gap
    cd /opt/gap4r7/local/pkg && hg clone https://sebasguts@bitbucket.org/gap-system/4ti2gap && cd 4ti2gap && ./autogen.sh && ./configure --with-gaproot=/opt/gap4r7 --with-4ti2=/usr/local && \
    make && \
    ## latest numericalsgrps
    cd /opt/gap4r7/local/pkg && hg clone https://sebasguts@bitbucket.org/gap-system/numericalsgps

ENTRYPOINT /bin/bash
